import Promotion from './promotion.js';

class HalfPricePromotion extends Promotion {
  constructor (order) {
    super(order);
    this._type = '指定菜品半价';
    this.halfPriceDishes = ['ITEM0001', 'ITEM0022'];
  }

  get type () {
    return this._type;
  }

  includedHalfPriceDishes () {
    // need to be completed
    return this.order.itemsDetails.filter(item => this.halfPriceDishes.includes(item.id));
  }

  discount () {
    // need to be completed
    // merge into one step;
    /*
    let discounts = 0;
    this.halfPriceDishes.forEach(halfPriceDish => {
      this.order.itemsDetails.forEach(item => {
        if (item.id === halfPriceDish) {
          discounts += item.price * item.count / 2;
        }
      });
    });
    return discounts;
    */
    return this.includedHalfPriceDishes()
      .map(item => item.price * item.count / 2)
      .reduce((a, b) => a + b, 0);
  }
}

export default HalfPricePromotion;
