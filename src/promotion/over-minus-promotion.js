import Promotion from './promotion.js';

class OverMinusPromotion extends Promotion {
  constructor (order) {
    super(order);
    this._type = '满30减6元';
  }

  get type () {
    return this._type;
  }

  discount () {
    // Need to be implemented
    // if-else merge into one sentence;
    /*
    if (super.originalPrice() >= 30) {
      return 6;
    } else {
      return 0;
    }
    */
    return super.originalPrice() >= 30 ? 6 : 0;
  }
}

export default OverMinusPromotion;
