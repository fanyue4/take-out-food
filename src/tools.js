function parseInputToMap (selectedItems) {
  // need to be implement
  const itemsMap = new Map();
  // slice parameters;
  selectedItems.split(',').forEach(item => {
    // itemsMap.set(item.slice(0, 8), Number.parseInt(item[item.length - 1]));
    const pair = item.split(' x ');
    itemsMap.set(pair[0], Number.parseInt(pair[1]));
  });
  return itemsMap;
}

export { parseInputToMap };
