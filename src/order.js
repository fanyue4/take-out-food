import dishes from './menu.js';

class Order {
  constructor (itemsMap) {
    // Need to be implement
    this.itemsMap = dishes.filter(dish => itemsMap.has(dish.id)).map(item => ({
      ...item,
      count: itemsMap.get(item.id)
    }));
  }

  get itemsDetails () {
    // Need to be implement
    return this.itemsMap;
  }

  get totalPrice () {
    // Need to be implement
    // for loop substituted;
    return this.itemsMap.map(item => item.price * item.count).reduce((a, b) => a + b);
  }
}

export default Order;
